package controller;

import model.Suma;
import model.Resta;
import model.Multiplicacion;
import model.Division;
/**
 *
 * @author lmvm
 */
public class CalculatorController {

    public CalculatorController() {

    }

    public float sumar(float number1, float number2) {

        Suma suma = new Suma();

        return suma.sumar(number1, number2);
    }

    public float restar(float number1, float number2) {

        Resta resta = new Resta();

        return resta.restar(number1, number2);
    }

    public float multiplicar(float number1, float number2) {
        
        Multiplicacion multiplicacion = new Multiplicacion();
        
        return multiplicacion.multiplicar(number1, number2);
    }
    
    public float dividir(float number1, float number2){
        
        Division division = new Division();
        
        return division.dividir(number1, number2);
    }
}
