package model;

/**
 *
 * @author lmvm
 */
public class Resta {
    
    public float restar(float number1, float number2){
        
        return number1 - number2;
    }
    
}
